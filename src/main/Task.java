import java.util.HashMap;
import java.util.Map;

public class Task {
    private String title;
    private String description;
    private static long uid;
    private long id;
    protected Enum<StatusTask> status;
    private HashMap<Long, SubTask> subTaskHashMap;

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
        this.id = uid++;
        status = StatusTask.NEW;
        subTaskHashMap = new HashMap<>();
    }

    public Task(String title) {
        this(title, null);
    }

    public long getId() {
        return id;
    }

    protected Enum<StatusTask> getStatus() {
        return status;
    }

    public void changeStatus(SubTask task) {
        if (subTaskHashMap.values().contains(task)) {
            task.editStatus(StatusTask.DONE);
            int lenght = 0;
            for (Map.Entry<Long, SubTask> entry : subTaskHashMap.entrySet()) {
                if (entry.getValue().getStatus() == StatusTask.DONE) {
                    lenght++;
                    if (lenght == subTaskHashMap.size()) {
                        status = StatusTask.DONE;
                        return;
                    }
                }
            }
            status = StatusTask.IN_PROGRESS;
        } else {
            System.out.println("Данный таск принадлежит другому списку или не найден");
        }
    }

    protected void setStatus(Enum<StatusTask> status) {
        this.status = status;
    }

    public void addSubTask(SubTask task) {
        subTaskHashMap.put(task.getId(), task);
        if (status == StatusTask.DONE)
            status = StatusTask.IN_PROGRESS;
    }

    public void removeSubTask(long id) {
        subTaskHashMap.remove(id);
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", id=" + id +
                ", status=" + status +
                ", subTaskHashMap=" + subTaskHashMap +
                '}';
    }
}

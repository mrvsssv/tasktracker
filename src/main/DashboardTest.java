public class DashboardTest {
    public static void main(String[] args) {
        Task task = new Task("Переезд", "Закончить отделку квартиры");
        Task task2 = new Task("Важный эпик 2", "Описание эпика");
        Task task3 = new Task("Важный эпик 2", "Описание эпика2");
        Task task4 = new Task("Список покупок", "Купить продукты");

        SubTask st = new SubTask("Майонез");
        SubTask st2 = new SubTask("Огурцы");
        SubTask st3 = new SubTask("Помидоры");
        SubTask st4 = new SubTask("Собрать коробки");
        SubTask st5 = new SubTask("Упаковать кошку");
        SubTask st6 = new SubTask("Сказать слова прощания");
        SubTask st7 = new SubTask("Задача 1");
        SubTask st8 = new SubTask("Задача 1");
        SubTask st9 = new SubTask("Задача 1");
        SubTask st10 = new SubTask("Задача 1");
        SubTask st1 = new SubTask("Задача 1");

        task.addSubTask(st4);
        task.addSubTask(st5);
        task.addSubTask(st6);

        task2.addSubTask(st7);
        task2.addSubTask(st8);

        task3.addSubTask(st9);
        task3.addSubTask(st10);

        task4.addSubTask(st);
        task4.addSubTask(st2);
        task4.addSubTask(st3);

        System.out.println(task);
        System.out.println(task2);
        System.out.println(task3);
        System.out.println(task4);

        System.out.println(task.getStatus());
        System.out.println(task2.getStatus());
        System.out.println(task3.getStatus());
        System.out.println(task4.getStatus());

        task.changeStatus(st9);
        task2.changeStatus(st7);
        task2.changeStatus(st8);
        task4.changeStatus(st);
        task4.changeStatus(st2);
        System.out.println("-------------------------------");

        System.out.println(task.getStatus());
        System.out.println(task2.getStatus());
        System.out.println(task3.getStatus());
        System.out.println(task4.getStatus());
        System.out.println("-------------------------------");

        task2.addSubTask(st1);
        System.out.println(task2.getStatus());

    }
}

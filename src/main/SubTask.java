public class SubTask extends Task{
    public SubTask(String title, String description) {
        super(title, description);
    }

    public SubTask(String title) {
        super(title);
    }

    public void editStatus(StatusTask statusTask) {
        this.setStatus(statusTask);
    }
}
